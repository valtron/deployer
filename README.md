# Deployer

Sample usage:

```python
from deployer.instance import RemoteInstance
from deployer.misc import Component, Python3, Git
from deployer.nginx import Nginx
from deployer.uwsgi import uWSGI

def main():
	inst = RemoteInstance('host.example.com', 'ubuntu',
		'/path/to/host.exame.com.pem',
		'/path/to/known_hosts',
	)
	
	component = inst.get_installed(MyComponent(inst))
	component.deploy()

class MyComponent(Component):
	def __init__(self, instance):
		super().__init__('mycomponent')
		self.instance = instance
	
	# Install is only run the first time
	def install(self):
		inst = self.instance
		
		# To build python libs
		with inst.sudo():
			inst.aptget_install('gcc')
			inst.aptget_install('python3-dev')
		
		proj = self._home() + '/project/' + self.name
		inst.ensure_dir(proj)
		
		git = inst.get_installed(Git(inst))
		git.clone('path@to.my:repo/name.git', proj)
		
		uwsgi = inst.get_installed(uWSGI(inst))
		uwsgi.add_vassal(self.name, proj + '/etc/uwsgi.ini')
		
		nginx = inst.get_installed(Nginx(inst))
		nginx.add_site(self.name, proj + '/etc/nginx.conf')
		nginx.enable_site(self.name)
		
		site = self._home() + '/site/' + self.name
		inst.ensure_dir(site)
		py3 = inst.get_installed(Python3(inst))
		py3.install_venv(site + '/venv')
	
	def deploy(self):
		inst = self.instance
		
		h = self._home()
		proj = h + '/project/' + self.name
		site = h + '/site/' + self.name
		
		git = inst.get_installed(Git(inst))
		git.pull(proj)
		
		with inst.sudo():
			inst.run('chown', '-R', 'ubuntu:ubuntu', site)
		
		to_push = (
			'folder1', 'folder2', 'file1.foo', 'file2.bar',
		)
		
		for x in to_push:
			inst.copy(proj + '/' + x, site + '/' + x)
		
		inst.ensure_dir(site + '/log')
		
		py3 = inst.get_installed(Python3(inst))
		
		venv = py3.venv(site + '/venv')
		venv.pip_install_reqs(proj + '/etc/requirements.txt')
		
		with inst.sudo():
			inst.run('chown', '-R', 'www-data:www-data', site)
			inst.get_installed(Nginx(inst)).restart()
			inst.get_installed(uWSGI(inst)).restart()
	
	def _home(self):
		return '/home/ubuntu'
```