from ..util import module_file
from ..misc import Component, SystemD

class Caddy(Component):
	def __init__(self, instance):
		super().__init__('caddy')
		self.instance = instance
	
	def install(self):
		inst = self.instance
		
		with inst.sudo():
			inst.run_raw('curl -s https://getcaddy.com | bash')
			inst.ensure_dir('/etc/caddy/sites-available')
			inst.ensure_dir('/etc/caddy/sites-enabled')
			inst.ensure_dir('/var/run/caddy')
			inst.run('addgroup', 'caddy')
			inst.run('useradd', '-r', '--create-home', '-g', 'caddy', '-s', '/bin/false', 'caddy')
			inst.run('chown', 'caddy:caddy', '/var/run/caddy')
			inst.run('setcap', 'cap_net_bind_service=+ep', '/usr/local/bin/caddy')
		
		inst.file_put(module_file(__file__, 'Caddyfile'), '/tmp/Caddyfile')
		inst.file_put(module_file(__file__, 'caddy.service'), '/tmp/caddy.service')
		
		with inst.sudo():
			inst.copy('/tmp/Caddyfile', '/etc/caddy/Caddyfile')
			systemd = inst.get_installed(SystemD(inst))
			systemd.add(self.name, '/tmp/caddy.service')
			systemd.enable(self.name)
			systemd.start(self.name)
	
	def add_site(self, name, conf):
		with self.instance.sudo():
			self.instance.symlink(conf, '/etc/caddy/sites-available/' + name)
	
	def enable_site(self, name):
		with self.instance.sudo():
			self.instance.symlink('/etc/caddy/sites-available/' + name, '/etc/caddy/sites-enabled/' + name)
	
	def disable_site(self, name):
		with self.instance.sudo():
			self.instance.remove('/etc/caddy/sites-enabled/' + name)

	def restart(self):
		systemd = self.instance.get_installed(SystemD(self.instance))
		systemd.restart(self.name)
