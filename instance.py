import subprocess
import paramiko
import shlex

class InstanceBase:
	def __init__(self):
		self._installed_dir = '/home/ubuntu/deployer/installed'
		self._sudo_height = 0
	
	def get_installed(self, component):
		if not self.is_installed(component):
			component.install()
			self.set_installed(component)
		return component
	
	def is_installed(self, component):
		return self.exists(self._installed_dir + '/' + component.name)
	
	def set_installed(self, component):
		self.ensure_dir(self._installed_dir)
		self.run('touch', self._installed_dir + '/' + component.name)
	
	def set_uninstalled(self, component):
		self.remove(self._installed_dir + '/' + component.name)
	
	def aptget_install(self, pkg):
		with self.sudo():
			self.run('apt-get', 'install', pkg, '-q', '-y')
	
	def symlink(self, target, source):
		self.run('ln', '-s', target, source)
	
	def ensure_dir(self, dir):
		self.run('mkdir', '-p', dir)
	
	def copy(self, src, dest):
		self.remove(dest)
		self.run('cp', '-r', src, dest)
	
	def remove(self, path):
		self.run('rm', '-rf', path)
	
	def exists(self, path):
		return self.run('test', '-e', path).exit_code == 0
	
	def run(self, cmd, *args, **kwargs):
		args = [cmd] + list(map(str, args))
		
		for k, v in kwargs.items():
			isb = isinstance(v, bool)
			if not isb or v is True:
				args.append(('--' if len(k) > 1 else '-') + k)
			if not isb:
				args.append(str(v))
		
		if self._sudo_height:
			args = ['sudo'] + args
		
		print("run", args)
		
		cmd_raw = self._cmd_to_raw(args)
		return self.run_raw(cmd_raw)
	
	def run_raw(self, cmd_raw):
		raise NotImplementedError('InstanceBase.run_raw')
	
	def sudo(self):
		return SudoContext(self)
	
	def file_put(self, local_source, remote_dest):
		raise NotImplementedError('InstanceBase.file_put')
	
	def file_get(self, remote_source, local_dest):
		raise NotImplementedError('InstanceBase.file_get')
	
	def _cmd_to_raw(self, args):
		return ' '.join(map(shlex.quote, args))

class LocalInstance(InstanceBase):
	def file_put(self, local_source, remote_dest):
		self.copy(local_source, remote_dest)
	
	def file_get(self, remote_source, local_dest):
		self.copy(remote_source, local_dest)
	
	def run_raw(self, cmd_raw):
		# TODO: Return Result
		return subprocess.run(cmd_raw)

class RemoteInstance(InstanceBase):
	def __init__(self, host, user, key, known_hosts):
		super().__init__()
		self._ssh = paramiko.SSHClient()
		self._ssh.load_host_keys(known_hosts)
		self._ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		self._ssh.connect(
			host, username = user, key_filename = key,
			allow_agent = False, look_for_keys = False, compress = True,
		)
	
	def file_put(self, local_source, remote_dest):
		print("file_put", local_source, remote_dest)
		sftp = self._ssh.open_sftp()
		sftp.put(local_source, remote_dest)
		sftp.close()
	
	def file_get(self, remote_source, local_dest):
		print("file_get", remote_source, local_dest)
		sftp = self._ssh.open_sftp()
		sftp.get(remote_source, local_dest)
		sftp.close()
	
	def run_raw(self, cmd_raw):
		ssh = self._ssh
		
		chan = ssh._transport.open_session()
		chan.settimeout(None)
		chan.exec_command(cmd_raw)
		#stdin = chan.makefile('wb', -1)
		stdout = chan.makefile('r', -1)
		stderr = chan.makefile_stderr('r', -1)
		
		stderr = stderr.readlines()
		stdout = stdout.readlines()
		
		return Result(stdout, stderr, chan.recv_exit_status())

class Result:
	def __init__(self, stdout, stderr, exit_code):
		self.stdout = stdout
		self.stderr = stderr
		self.exit_code = exit_code

class SudoContext:
	def __init__(self, instance):
		self.instance = instance
	
	def __enter__(self):
		self.instance._sudo_height += 1
	
	def __exit__(self, type, value, traceback):
		self.instance._sudo_height -= 1
