class Component:
	def __init__(self, name):
		self.name = name
	
	def install(self):
		raise NotImplementedError('Component.install')

class AptGetComponent(Component):
	def __init__(self, name, instance):
		super().__init__(name)
		self.instance = instance
	
	def install(self):
		self.instance.aptget_install(self.name)

class Upstart(AptGetComponent):
	def __init__(self, instance):
		super().__init__('upstart', instance)
	
	def add(self, name, conf):
		self.instance.copy(conf, '/etc/init/' + name + '.conf')
	
	def start(self, name):
		self.instance.run('service', 'start', name)

class SystemD(AptGetComponent):
	def __init__(self, instance):
		super().__init__('systemd', instance)
	
	def add(self, name, conf):
		self.instance.copy(conf, '/etc/systemd/system/' + name + '.service')
	
	def start(self, name):
		self.instance.run('systemctl', 'start', name + '.service')
	
	def enable(self, name):
		self.instance.run('systemctl', 'enable', name + '.service')
	
	def restart(self, name):
		self.instance.run('systemctl', 'restart', name + '.service')

class Pip3(AptGetComponent):
	def __init__(self, instance):
		super().__init__('python3-pip', instance)
	
	def install_package(self, name):
		self.instance.run('pip3', 'install', name)
	
	def install_reqs(self, filename):
		self.instance.run('pip3', 'install', requirement = filename)

class Git(AptGetComponent):
	def __init__(self, instance):
		super().__init__('git', instance)
	
	def clone(self, repo, dest):
		self.instance.run('git', 'clone', repo, dest)
	
	def pull(self, dir):
		self.instance.run('git', '-C', dir, 'pull')

class Python3(AptGetComponent):
	def __init__(self, instance):
		super().__init__('python3', instance)
	
	def install(self):
		super().install()
		self.instance.aptget_install('python-virtualenv')
	
	def install_venv(self, dir):
		self.instance.run('virtualenv', '-p', 'python3', dir)
	
	def venv(self, dir):
		return PyVenv(dir, self.instance)

class PyVenv:
	def __init__(self, dir, instance):
		self.dir = dir
		self.instance = instance
	
	def pip_install_reqs(self, filename):
		self.instance.run(self.dir + '/bin/pip', 'install', requirement = filename)

class Certbot(AptGetComponent):
	def __init__(self, instance):
		super().__init__('certbot', instance)
	
	def certonly(self, config_file):
		self.instance.run(self.name, 'certonly', '--config', config_file)
