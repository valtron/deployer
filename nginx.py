from .misc import AptGetComponent, SystemD

class Nginx(AptGetComponent):
	def __init__(self, instance):
		super().__init__('nginx', instance)
	
	def install(self):
		super().install()
		self.disable_site('default')
	
	def restart(self):
		systemd = self.instance.get_installed(SystemD(self.instance))
		systemd.restart(self.name)
	
	def add_site(self, name, conf):
		with self.instance.sudo():
			self.instance.symlink(conf, '/etc/nginx/sites-available/' + name)
	
	def enable_site(self, name):
		with self.instance.sudo():
			self.instance.symlink('/etc/nginx/sites-available/' + name, '/etc/nginx/sites-enabled/' + name)
	
	def disable_site(self, name):
		with self.instance.sudo():
			self.instance.remove('/etc/nginx/sites-enabled/' + name)
