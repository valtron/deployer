from ..util import module_file
from ..misc import SystemD, Pip3, Component

class uWSGI(Component):
	def __init__(self, instance):
		super().__init__('uwsgi')
		self.instance = instance
	
	def install(self):
		inst = self.instance
		
		inst.aptget_install('uwsgi-plugin-python3')
		
		inst.file_put(module_file('emperor.ini'), '/tmp/uwsgi-emperor.ini')
		
		systemd = inst.get_installed(SystemD(inst))
		inst.file_put(module_file('uwsgi.service'), '/tmp/uwsgi.service')
		
		with inst.sudo():
			inst.ensure_dir('/etc/uwsgi/vassals')
			
			inst.copy('/tmp/uwsgi-emperor.ini', '/etc/uwsgi/emperor.ini')
			systemd.add(self.name, '/tmp/uwsgi.service')
			
			inst.run('mkdir', '/var/log/uwsgi')
			inst.run('chown', 'www-data:www-data', '/var/log/uwsgi')
			
			systemd.enable(self.name)
			systemd.start(self.name)
	
	def add_vassal(self, name, inifile):
		with self.instance.sudo():
			f = '/etc/uwsgi/vassals/' + name + '.ini'
			self.instance.symlink(inifile, f)
			self.instance.run('chown', 'www-data:www-data', f)
	
	def restart(self):
		systemd = self.instance.get_installed(SystemD(self.instance))
		systemd.restart(self.name)
